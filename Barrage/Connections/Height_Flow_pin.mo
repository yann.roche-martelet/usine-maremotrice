within Barrage.Connections;

connector Height_Flow_pin "Height_pin"
  Modelica.Units.SI.Height h "Height at the pin" annotation (unassignedMessage="Height unassigned");
  flow Modelica.Units.SI.VolumeFlowRate Q "Volumic Flow through the pin" annotation (unassignedMessage="Flow unassigned");
  annotation (defaultComponentName="pin",
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
            100}}), graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid)}),
    Diagram(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{
            100,100}}), graphics={Rectangle(
          extent={{-40,40},{40,-40}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-160,110},{40,50}},
          textColor={0,0,255},
          textString="%name")}));
end Height_Flow_pin;

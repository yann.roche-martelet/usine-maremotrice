within Barrage;

model TidalPowerSystem
  Composants.Mer mer annotation(
    Placement(transformation(origin = {82, 6}, extent = {{-10, -10}, {10, 10}})));
  Composants.ProductionUnit productionUnit annotation(
    Placement(transformation(origin = {6, 6}, extent = {{-10, -10}, {10, 10}})));
  Composants.Bassin bassin annotation(
    Placement(transformation(origin = {-68, 12}, extent = {{-10, -10}, {10, 10}})));
  Composants.VanneMer vanneMer annotation(
    Placement(transformation(origin = {42, 6}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(productionUnit.Sea_pin, vanneMer.turbine_pin) annotation(
    Line(points = {{16, 6}, {32, 6}}, color = {0, 0, 255}));
  connect(vanneMer.Sea_pin, mer.pin) annotation(
    Line(points = {{52, 6}, {72, 6}}, color = {0, 0, 255}));
  connect(bassin.pin, productionUnit.Pond_pin) annotation(
    Line(points = {{-58, 12}, {-4, 12}, {-4, 6}}, color = {0, 0, 255}));
end TidalPowerSystem;

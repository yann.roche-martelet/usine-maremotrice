within Barrage.Composants;

model Bassin
extends Connections;
  
  Real V(unit= "m^3");
  Real hb(unit="m");

  parameter Real alpha(unit = "m^(-1)") = 6.14e-7;
  parameter Real beta(unit = "m") = 2.46;

  Height_Flow_pin pin annotation(
    Placement(transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}})));    

initial equation

  V = 0; // Initial volume
  hb= 0;
equation

  der(V) = pin.Q; // Flow rate defines the change in volume
// Calculate hb based on V, ensuring non-negative square root
  hb = sqrt(2* alpha * V + beta^2 ) - beta;

  pin.h=hb ;

end Bassin;

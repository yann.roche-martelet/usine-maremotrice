within Barrage.Composants;

model Mer
extends Connections;

  parameter Real T(unit= "s") = 3600*12;
  parameter Real P0(unit="Pa") = 1e5;
  parameter Real hmx(unit="m") = 13.5;
  constant Real PI = Modelica.Constants.pi;
  Height_Flow_pin pin annotation(
    Placement(transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}})));

equation

  pin.h = hmx/2 + hmx/2*cos(2*PI*time/T);

end Mer;

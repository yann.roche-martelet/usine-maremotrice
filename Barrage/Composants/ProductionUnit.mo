within Barrage.Composants;
model ProductionUnit
extends Connections;
Real Phyd;
Real Pmec;
Real Pelec;

Real DeltH;
Real DeltHalg;
Real DeltP;

Real g(unit="m/s^2")=9.81; //gravitational acceleration of the earth
Real Rho0(unit="kg/m^3")=1000; //volumic mass of water

Real Alpha;
Real Beta;

parameter Real Alpha_PondToSea(unit="m^(5/2)/s") = 4.49419*10^(-4) "1er Coefficient de calcul du débit si le réservoir se remplit";
parameter Real Alpha_SeaToPond(unit="m^(5/2)/s") = 5.71715*10^(-4) "1er Coefficient de calcul du débit si le réservoir se vide";
parameter Real Beta_PondToSea(unit="m^(5/2)/s") = 1.51079*10^(-2) "2e Coefficient de calcul du débit si le réservoir se remplit";
parameter Real Beta_SeaToPond(unit="m^(5/2)/s") = 3.37798*10^(-2) "2e Coefficient de calcul du débit si le réservoir se vide";

parameter Real Nbul(unit="Bulbe(s)") = 24 "Nombre de bulbes de production";

parameter Real Rend_Tur(unit="sans unité") = 0.9 "Rendement de la Turbine";
parameter Real Rend_Alt(unit="sans unité") = 0.95 "Rendement de l'alternateur";

  Height_Flow_pin Pond_pin annotation(
    Placement(transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}})));
  Height_Flow_pin Sea_pin annotation(
    Placement(transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}})));

equation

DeltHalg = Pond_pin.h-Sea_pin.h;
DeltH = abs(DeltHalg);

Alpha = if Pond_pin.h > Sea_pin.h then Alpha_PondToSea else Alpha_SeaToPond;
Beta = if Pond_pin.h > Sea_pin.h then Beta_PondToSea else Beta_SeaToPond;

Sea_pin.Q = if Pond_pin.h > Sea_pin.h then -Nbul*(-Beta+sqrt(Beta^2+4*DeltH*Alpha))/(2*Alpha) else Nbul*(-Beta+sqrt(Beta^2+4*Alpha*DeltH))/(2*Alpha); //Volumic Flow entering the Sea
Pond_pin.Q = -Sea_pin.Q; //Volumic Flow entering the pond

DeltP = Rho0*g*DeltH;

Phyd = DeltP*abs(Sea_pin.Q);
Pmec = Rend_Tur*Phyd;
Pelec = Rend_Alt*Pmec;



annotation(
    uses(Modelica(version = "4.0.0")),
    Icon);
end ProductionUnit;

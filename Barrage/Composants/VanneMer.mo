within Barrage.Composants;

model VanneMer
extends Connections;
parameter Boolean isOpen(start=true);
  Height_Flow_pin turbine_pin annotation(
    Placement(transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}})));
  Height_Flow_pin Sea_pin annotation(
    Placement(transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}})));

equation
  
if not isOpen then
  turbine_pin.h=0;
  Sea_pin.Q= -turbine_pin.Q;
else
  turbine_pin.h=Sea_pin.h;
  Sea_pin.Q= -turbine_pin.Q;
end if;
end VanneMer;
